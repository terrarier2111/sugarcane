use crate::{block, player::Player, plugin::EventResult, world::WorldManager};
use libloading::{Error, Library, Symbol};
use sc_common::{math::Pos, net::sb::ClickWindow};
use std::sync::Arc;

macro_rules! plugin {
  ( $($field:ident: $ty:ty,)* ) => {
    pub struct RustPlugin {
      _library: &'static Library,
      pub(crate) name: String,
      $(
        pub(crate) $field: Option<Symbol<'static, $ty>>,
      )*
    }

    impl RustPlugin {
      pub(crate) fn new(source: Library, world_manager: Arc<WorldManager>) -> Option<Self> {
        let source = Box::leak(Box::new(source));
        let name: Option<Symbol<fn() -> String>> = unsafe { source.get(NAME) }.ok();
        let on_init: Option<Symbol<fn(/*Arc<WorldManager>*/)>> = unsafe { source.get(INIT) }.ok();
        $(
          let $field: Option<Symbol<'static, $ty>> = unsafe { source.get(stringify!($field).as_bytes().clone()) }.ok();
        )*
        if name.is_none() || on_init.is_none() {
          return None;
        }
        let name: String = name.unwrap()();
        on_init.unwrap()(/*world_manager*/);
        Some(Self {
          _library: source,
          name,
          $(
            $field,
          )*
        })
      }

    }
  }
}

const NAME: &[u8] = b"get_name"; // This function has to be PRESENT in every rust plugin
const INIT: &[u8] = b"on_init"; // This function has to be PRESENT in every rust plugin
const BLOCK_PLACE: &[u8] = b"on_block_place"; // This function is OPTIONAL
const WINDOW_CLICK: &[u8] = b"on_window_click"; // This function is OPTIONAL

plugin!(
  block_place: fn(Arc<Player>, pos: Pos, kind: block::Kind) -> EventResult,
  window_click: fn(Arc<Player>, i32, ClickWindow) -> EventResult,
);
